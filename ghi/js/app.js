// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok){
//             const details = await detailResponse.json();
//             const title = details.conference.title;
//             const description = details.conference.description;
//         const imageTag = document.querySelector('.card-img-top');
//         imageTag.src = details.conference.location.picture_url;
//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });

function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitles mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${startDate}-${endDate}
      </div>
    </div>
  `;
}

function convert(stringDate){
  let strDate = new Date(stringDate).toLocaleDateString('en-us', {year:"numeric", month:"short", day:"numeric"});
  return strDate
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';
  let columnIndex = 0 //starts column index at 0

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = convert(details.conference.starts);
          const endDate = convert(details.conference.ends);
          const location = details.conference.location.name;
          const html = createCard(name, description, pictureUrl, startDate, endDate, location);

          if (columnIndex >= 3){
            columnIndex = 0;
          }

          const row = document.querySelector('.row')
          const column = row.children[columnIndex];
          column.innerHTML += html;

          columnIndex++;
        }
      }

    }
  } catch (e) {
    console.log("error: ", e);
  }

});
